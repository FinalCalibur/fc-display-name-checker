package scripts.fc.fcdisplaynamechecker;

public class GUI extends javax.swing.JFrame
{
	private static final long serialVersionUID = 7093908833013294721L;
	
	private FCDisplayNameChecker script;
	
	public GUI(FCDisplayNameChecker script)
	{
		this.script = script;
		
		initComponents();
	}

	private void initComponents()
	{

		titleLabel = new javax.swing.JLabel();
		lettersSpinner = new javax.swing.JSpinner();
		lettersLabel = new javax.swing.JLabel();
		realWordsCheckBox = new javax.swing.JCheckBox();
		startButton = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("FC DNC");
		setAlwaysOnTop(true);

		titleLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		titleLabel.setText("FC Display Name Checker");

		lettersLabel.setText("Letters:");

		realWordsCheckBox.setText("Real words?");

		startButton.setText("Start");
		startButton.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				startButtonActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(titleLabel)
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
				.addGroup(
						layout.createSequentialGroup()
								.addGap(41, 41, 41)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												false)
												.addComponent(
														realWordsCheckBox,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		lettersLabel)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		lettersSpinner))
												.addComponent(
														startButton,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE))
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(titleLabel)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(
														lettersSpinner,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(lettersLabel))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(realWordsCheckBox)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										12, Short.MAX_VALUE)
								.addComponent(startButton)));

		pack();
	}

	private void startButtonActionPerformed(java.awt.event.ActionEvent evt)
	{
		script.setNameLetters((int)lettersSpinner.getValue());
		
		script.setUsingRealWords(realWordsCheckBox.isSelected());
		
		dispose();
	}

	// Variables declaration - do not modify
	private javax.swing.JLabel lettersLabel;
	private javax.swing.JSpinner lettersSpinner;
	private javax.swing.JCheckBox realWordsCheckBox;
	private javax.swing.JButton startButton;
	private javax.swing.JLabel titleLabel;
	// End of variables declaration
}
