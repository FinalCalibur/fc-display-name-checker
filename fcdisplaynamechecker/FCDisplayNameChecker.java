package scripts.fc.fcdisplaynamechecker;

import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import org.tribot.util.Util;

import scripts.fc.api.social.IgnoreList;
import scripts.fc.api.utils.Utils;
import scripts.fc.framework.paint.FCPaint;
import scripts.fc.framework.paint.FCPaintable;
import scripts.fc.framework.script.FCScript;

@ScriptManifest(
		authors     = { 
		    "Final Calibur"
		}, 
		category    = "Tools", 
		name        = "FC Display Name Checker", 
		version     = 0.2, 
		description = "Checks display names", 
		gameMode    = 1)


public class FCDisplayNameChecker extends FCScript implements Starting, Ending, Painting, FCPaintable
{
	//Local constants
	private final int SLEEP_TIME = 20;
	private final int 	MAX_IGNORE_LIST_SIZE = 5;	
	private final String[] ALPHABET = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
	private final String	OUTPUT_FILE = Util.getWorkingDirectory() + "/fcnames.txt";
	private final String	BLACKLIST_FILE = Util.getWorkingDirectory() + "/fcblacklist.txt";
	private final String 	DICTIONARY_FILE = "http://158.69.112.148/docs/dictionary.txt";
	private final ScriptManifest MANIFEST = (ScriptManifest)this.getClass().getAnnotation(ScriptManifest.class); //The script manifest object

	//Local variables
	private int nameLetters = -1;
	private int namesChecked;
	private int namesAvailable;
	private int ignoreListSize;
	private boolean usingRealWords;
	private String nameToCheck;
	private String status;
	private List<String> realWords = new ArrayList<String>();
	private List<String> checked = new ArrayList<String>();
	private FCPaint paint = new FCPaint(this, Color.CYAN);
	private GUI gui = new GUI(this);
	
	
	@Override
	protected int mainLogic()
	{
		if(Login.getLoginState() == STATE.INGAME && nameLetters > 0)
		{
			if(usingRealWords && realWords.isEmpty())
				loadRealWords();
			
			if(usingRealWords && realWords.isEmpty())
			{
				println("Ran out of real words to check! Ending script...");
				
				return -1;
			}
			
			//Add previous name to blacklist
			checked.add(nameToCheck);
			
			//Clear ignore list if too full
			if(ignoreListSize > MAX_IGNORE_LIST_SIZE)
			{
				status = "Clearing ignore list";
				
				IgnoreList.removeAll();
			}
			
			//Generate name
			status = "Generating name";	
			
			nameToCheck = generateName();
						
			//Add name to ignore list
			status = "Adding name to ignore list";
			
			ignoreListSize = IgnoreList.getNames().size();
			
			sleep(500, 800);
			
			IgnoreList.addName(nameToCheck);
			
			if(!nameWasAdded(ignoreListSize))
			{
				//Write name to text file
				status = "Writing name to text file";
				
				General.println("Name available: " + nameToCheck);
				
				namesAvailable++;
				
				output(nameToCheck, OUTPUT_FILE);
			}
			else
			{ 
				//Remove name from ignore list
				status = "Removing name from list";
				
				IgnoreList.removeName(nameToCheck);
			}
			
			namesChecked++;
		}
		
		return SLEEP_TIME;
	}

	@Override
	public void onEnd() 
	{
		println("Thank you for running " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
	}

	@Override
	public void onStart() 
	{
		//display start up message
		println("Started " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
		
		loadBlacklist();
		
		Utils.handleGui(gui);
		
		status = "Starting up";
	}
	
	private void loadRealWords()
	{
		try
		{
			URL url = new URL(DICTIONARY_FILE);
			
	        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	        {
	        	if(inputLine.length() == nameLetters && !checked.contains(inputLine))
					realWords.add(inputLine);
	        }
	        
	        in.close();
			
			println("Added " + realWords.size() + " unchecked words to real words list.");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void loadBlacklist()
	{
		try
		{
			File blacklistFile = new File(BLACKLIST_FILE);
			
			if(blacklistFile.exists())
			{
				FileReader fr = new FileReader(blacklistFile);
				BufferedReader br = new BufferedReader(fr);
				
				String line = br.readLine();
				
				while(line != null)
				{
					checked.add(line);
					
					line = br.readLine();
				}
				
				println("Added " + checked.size() + " names to blacklist.");
				
				br.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void output(String name, String filePath)
	{
		try
		{
			FileWriter fw = new FileWriter(filePath, true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(name);
			
			bw.newLine();
			
			bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private boolean nameWasAdded(final int oldSize)
	{
		return Timing.waitCondition(new Condition()
		{
			@Override
			public boolean active() 
			{
				sleep(10);
				
				return IgnoreList.getNames().size() > oldSize;
			}
			
		}, General.random(3000, 5000));
	}
	
	private String generateName()
	{
		String name = "";
		
		if(usingRealWords)
			name = realWords.remove(0);
		else
		{
			do
			{
				name = "";
				
				for(int i = 0; i < nameLetters; i++)
				{
					if(General.random(0, 1) == 0) //Letter
						name += ALPHABET[General.random(0, ALPHABET.length - 1)];
					else //Number
						name += General.random(0, 9);
				}
			}
			while(checked.contains(name));
		}
		
		output(name, BLACKLIST_FILE);
		
		return name;
	}

	@Override
	public void onPaint(Graphics g) 
	{
		paint.paint(g);	
	}

	public void setNameLetters(int letters)
	{
		this.nameLetters = letters;
	}
	
	public void setUsingRealWords(boolean bool)
	{
		this.usingRealWords = bool;
	}

	@Override
	protected String[] scriptSpecificPaint()
	{
		return new String[] {"Status: " + status, 
				"Names Checked: " + namesChecked, "Names Available: " + namesAvailable, 
				"Names P/H: " + paint.getPerHour(namesChecked)};
	}
}
